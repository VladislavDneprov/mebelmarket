<?php
/**
 * Author: Vladislav Dneprov
 * Author URI: https://www.linkedin.com/in/vladislav-dneprov
 * Date: 05.12.2016 13:16
 * Version: 1.0.0
 */

$categories = get_terms(array(
    'taxonomy'          => 'product-category',
    'order'             => 'DESC',
    'hide_empty'        => false,
    'childless'          => false,
));
?>

<div class="sidebar-block sidebar-category">
    <div class="title">Категории</div>
    <ul>
        <?php foreach ($categories as $number => $category): ?>
            <?php
                $children = get_term_children($category->term_id, 'product-category');
            ?>
            <?php if(!$category->parent): ?>
                <li>
                    <a href="<?= get_term_link($category->term_id); ?>"><?= $category->name; ?></a>
                    <?php if ($children): ?>
                        <span class="dropdown-sub fa fa-plus"></span>
                        <ul>
                            <?php foreach ($children as $id): ?>
                                <?php $parent_category = get_term($id); ?>
                                <li><a title="<?= $parent_category->name; ?>" href="<?= get_term_link($id); ?>"><?= $parent_category->name; ?></a></li>
                            <?php endforeach; ?>
                        </ul>
                    <?php endif; ?>
                </li>
            <?php endif; ?>
        <?php endforeach; ?>
    </ul>
</div>