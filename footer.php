<?php
/**
 * Шаблон подвала (footer.php)
 * @package WordPress
 * @subpackage money-online
 *
 * wp_nav_menu(array('theme_location' => 'footer', 'menu_class' => 'col-lg-4 col-md-4 col-sm-4 col-xs-12','container' => ''))
 */

?>
	<footer>

		<div class="container-fluid newsletter">

			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
						<button data-toggle="modal" data-target="#modal-callback" class="btn btn-action">Заказать звонок</button>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 social">
						<ul>
							<li class="vkontakte"><a href="<?= get_theme_mod('information_social_vk', '#'); ?>"></a></li>
							<li class="odnoclass"><a href="<?= get_theme_mod('information_social_odnoclass', '#'); ?>"></a></li>
						</ul>
					</div>
				</div>
			</div>

		</div>

		<div class="container-fluid">

			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
						<div class="row footer-info">

							<?php
								$categories = get_terms(array(
									'taxonomy'          => 'product-category',
									'order'             => 'DESC',
									'hide_empty'        => false,
									'childless'         => false,
									'number'			=> 10
								));
							?>
							<?php if ($categories): ?>
								<!-- category footer -->
								<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
									<h4>Категории</h4>
									<ul>
										<?php foreach ($categories as $number => $category): ?>
											<li><a href="<?= get_term_link($category->term_id); ?>"><?= $category->name; ?></a></li>
										<?php endforeach; ?>
									</ul>
								</div>
								<!-- /category footer -->
							<?php endif; ?>

							<?php
								$args_new = array(
									'numberposts'     => 10,
									'orderby'         => 'date',
									'order'           => 'DESC',
									'post_type'       => 'product',
									'post_status'     => 'publish'
								);
								$products_news = get_posts($args_new);
							?>
							<?php if ($products_news): ?>
								<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
									<h4>Новые товары</h4>
									<ul>
										<?php foreach ($products_news as $number => $product): ?>
											<li><a href="<?= get_permalink($product->ID); ?>"><?= $product->post_title; ?></a></li>
										<?php endforeach; ?>
									</ul>
								</div>
							<?php endif; ?>

						</div>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 footer-contact">
						<h4>Информация о магазине</h4>
						<p><?= get_theme_mod('information_country', 'Украина'); ?>, <?= get_theme_mod('information_city', 'г.Харьков'); ?>, <?= get_theme_mod('information_index', '61162'); ?><br><?= get_theme_mod('information_first_address', 'ул.Оддеская 59 (второй этаж)'); ?><br><?= get_theme_mod('information_second_address', 'ст.м.Проспект Гагарина'); ?></p>
						<h4>Звоните нам:</h4>
						<p class="phone"><?= get_theme_mod('information_first_tel', '(011)-111-11-11'); ?></p>
						<p>Email: <a href="mailto:<?= get_theme_mod('information_email', '(011)-111-11-11'); ?>"><?= get_theme_mod('information_email', '(011)-111-11-11'); ?></a></p>
					</div>
				</div>
			</div>

		</div>
		<div class="container-fluid footer-bottom">

			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
						&#169; <?= date('Y'); ?> mebel-market.kh.ua
					</div>
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
					</div>
				</div>
			</div>

		</div>
		<?php wp_footer(); ?>
	</footer>


	<div class="modal fade" id="modal-order" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4>Оформить заказ</h4>
				</div>
				<div class="modal-body">
					<?= do_shortcode('[contact-form-7 id="101" title="Заказ товара"]'); ?>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
	<!-- /.modals -->

	<div class="modal fade" id="modal-callback" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4>Заказ обратного звонка</h4>
				</div>
				<div class="modal-body">
					<?= do_shortcode('[contact-form-7 id="100" title="Обратный звонок"]'); ?>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
	<!-- /.modals -->
</html>