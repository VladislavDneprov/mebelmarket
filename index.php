<?php 
/**
 * Template Name: Home
 * @package WordPress
 * @subpackage g-r
 */
get_header(); ?>


<div class="container-fluid">
	<div id="home" class="container">
	
		<div class="row">
			<!-- sidebar left -->
			<aside class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
				<?php get_sidebar('left'); ?>
			</aside>
			<!-- /sidebar left -->

			<section class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<?php get_template_part('part/home-slider'); ?>
					</div>
				</div>

				<div class="row list-products">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<?php get_template_part('part/feature-products'); ?>
					</div>
				</div>
			</section>
		</div>

    </div>

	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><h3>Наши партнёры</h3></div>
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 partner-slider">
				<?php
				$args_slide = array(
					'numberposts'     => -1,
					'order'           => 'DESC',
					'post_type'       => 'partner',
					'post_status'     => 'publish'
				);

				$slides = get_posts($args_slide);

				foreach ($slides as $slide):
				?>
				<div class="partner-slider-cotainer">
					<a title="<?= $slide->post_title; ?>" href="<?= get_post_meta($slide->ID, 'url', 1) ?>">
						<?php if(get_the_post_thumbnail($slide->ID)): ?>
							<img alt="<?= $slide->post_title; ?>" src="<?= get_the_post_thumbnail_url($slide->ID, 'full'); ?>" />
							<?php else: ?>
								<h5><?= $slide->post_title; ?></h5>
						<?php endif; ?>
					</a>
				</div>
				<?php endforeach; ?>
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>