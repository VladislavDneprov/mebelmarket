jQuery(document).ready(function($){

	/** mask **/
	$('#input_tel').inputmask("+380(99)-999-99-99");  //static mask
	
	$('.images-product .container-list-images img').click(function () {
		var src = $(this).attr('src');
		$('.images-product .main-image img').attr('src', src);
		$('.images-product .main-image img').attr('srcset', src);
	});

	$('.sidebar-category > ul > li').click(function () {
		$(this).find('ul').toggleClass('active');
		$(this).toggleClass('active');

		if ($(this).find('ul').hasClass('active')) {
			$(this).find('ul').show(300);
			$(this).find('.dropdown-sub').removeClass('fa-plus').addClass('fa-minus');
		}
		else {
			$(this).find('ul').hide(300);
			$(this).find('.dropdown-sub').removeClass('fa-minus').addClass('fa-plus');
		}
	});

	$('.partner-slider').slick({
		//dots: true,
		infinite: false,
		arrows: false,
		speed: 500,
		autoplay: true,
		autoplaySpeed: 3000,
		slidesToShow: 6,
		responsive: [
			{
				breakpoint: 1024,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 3,
					infinite: true,
					dots: true
				}
			},
			{
				breakpoint: 600,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2
				}
			},
			{
				breakpoint: 480,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}
		]
	});
	$('.slider-home').slick({
		dots: true,
		infinite: false,
		arrows: false,
		speed: 500,
		//autoplay: true,
		autoplaySpeed: 2000,
		slidesToShow: 1,
		responsive: [
			{
				breakpoint: 1024,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 3,
					infinite: true,
					dots: true
				}
			},
			{
				breakpoint: 600,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2
				}
			},
			{
				breakpoint: 480,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}
		]
	});
	$('.list-images').slick({
		//dots: true,
		infinite: false,
		arrows: true,
		speed: 500,
		//autoplay: true,
		autoplaySpeed: 2000,
		slidesToShow: 3,
		nextArrow: '<span class="slick-next fa fa-arrow-right"></span>',
		prevArrow: '<span class="slick-prev fa fa-arrow-left"></span>',
		responsive: [
			{
				breakpoint: 1024,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 3,
					infinite: true,
					dots: true
				}
			},
			{
				breakpoint: 600,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2
				}
			},
			{
				breakpoint: 480,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}
		]
	});
});