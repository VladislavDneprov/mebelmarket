<?php
/**
 * Author: Vladislav Dneprov
 * Author URI: https://www.linkedin.com/in/vladislav-dneprov
 * Date: 30.11.2016 13:57
 * Version: 1.0.0
 */


setPostViews(get_the_ID());
get_header();

?>

<div id="single-product" class="container-fluid">
    <div class="container">

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 breadcrumb">
                <?php get_template_part('part/breadcrumbs'); ?>
            </div>
        </div>

        <?php if (have_posts()): ?>
        <div class="row single-product-container">
            <?php while (have_posts()): the_post(); ?>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 images-product">

                    <?php if(get_the_post_thumbnail(get_the_id())): ?>
                        <div class="main-image">
                            <?= get_the_post_thumbnail(get_the_id(), 'full'); ?>
                        </div>
                    <?php endif; ?>

                    <?php if(get_post_meta(get_the_id(), 'images', 1) || get_the_post_thumbnail(get_the_id())): ?>
                        <div class="list-images">
                            <div class="container-list-images">
                                <?= get_the_post_thumbnail(get_the_id(), 'full'); ?>
                            </div>
                            <?php if(json_decode(get_post_meta(get_the_id(), 'images', 1))): ?>
                                <?php foreach (json_decode(get_post_meta(get_the_id(), 'images', 1)) as $image): ?>
                                    <div class="container-list-images">
                                        <img src="<?= $image; ?>" alt="" />
                                    </div>
                                <?php  endforeach; ?>
                            <?php endif; ?>
                        </div>
                    <?php endif; ?>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 info-product">
                    <div class="title"><h3><?= get_the_title(); ?></h3></div>
                    <div class="desc"><?= get_post_meta(get_the_id(), 'short_desc', 1); ?></div>
                    <div class="price">
                        <?php if (get_post_meta( get_the_id(), 'price_sale', 1 ) && get_post_meta( get_the_id(), 'price', 1 )): ?>
                            Цена:
                            <?php
                                $sale = (get_post_meta( get_the_id(), 'price', 1 ) - get_post_meta( get_the_id(), 'price_sale', 1 )) / (get_post_meta( get_the_id(), 'price', 1 ) / 100);
                                $sale = round($sale, 0, PHP_ROUND_HALF_UP);
                            ?>
                            <span class="new-price"><?= number_format(get_post_meta( get_the_id(), 'price_sale', 1 ), 2, '.', ' '); ?> грн.</span> <span class="price-percent-reduction">-<?= $sale ?>%</span>
                            <del class="old-price"><?= number_format(get_post_meta( get_the_id(), 'price', 1 ), 2, '.', ' '); ?> грн.</del>
                        <?php else: ?>
                            <?php if(get_post_meta(get_the_id(), 'price', 1)): ?>
                                Цена: <span class="new-price"><?= number_format(get_post_meta(get_the_id(), 'price', 1), 2, '.', ' '); ?> грн.</span>
                            <?php endif; ?>
                        <?php endif; ?>
                    </div>
                    <div class="action">
                        <button data-toggle="modal" data-target="#modal-order" class="btn btn-info">Быстрый заказ</button>
                    </div>
                    <div class="harak">
                        <?php if(get_post_meta(get_the_id(), 'product_harak', 1)): ?>
                            <h4>Характеристики</h4>
                            <ul>
                                <?php foreach (json_decode(get_post_meta(get_the_id(), 'product_harak', 1)) as $name => $value): ?>
                                    <li>
                                        <div class="name"><?= $value->name; ?></div>
                                        <div class="value"><?= $value->value; ?></div>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        <?php endif; ?>
                    </div>
                    <div class="full-desc">
                        <?php if(get_post_meta(get_the_id(), 'full_desc', 1)): ?>
                            <h4>Описание</h4>
                            <?= get_post_meta(get_the_id(), 'full_desc', 1); ?></p>
                        <?php endif; ?>
                    </div>
                </div>
            <?php endwhile; ?>
        </div>
        <?php endif; ?>

    </div>
</div>

<?php get_footer(); ?>
