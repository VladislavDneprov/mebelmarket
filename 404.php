<?php 
/**
 * @package WordPress
 * @subpackage g-r
 */
get_header(); ?>

<div class="container-fluid">
	<div id="not-found" class="container">

		<div class="row">
			<!-- sidebar left -->
			<aside class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
				<?php get_sidebar('left'); ?>
			</aside>
			<!-- /sidebar left -->

			<section class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
				<h2>Страница не найдена</h2>
				<p><a href="<?= get_home_url(); ?>" title="На главную"><span class="fa fa-home"></span> Вернуться на главную страницу</a></p>
			</section>
		</div>

	</div>
</div>

<?php get_footer(); ?>