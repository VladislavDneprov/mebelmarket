<?php
/**
 * wp_nav_menu(array('theme_location' => 'languages', 'container_class' => 'lang dropdown','container' => 'div')) - Top menu
 */
?>
<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php wp_title(); ?></title>
        <link href="https://fonts.googleapis.com/css?family=PT+Sans" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Lora" rel="stylesheet">
        <?php wp_head(); ?>
    </head>
    
    <body>
        <header>
            <div id="header" class="container-fluid">
                <div class="container">
                    <div class="row">

                        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 header-logo">
                            <a title="Mebel Market" href="<?= get_home_url(); ?>">
                                <h1>Mebel Market</h1>
                                <h5>Магазин современной мебели</h5>
                            </a>
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9 header-info">
                            <div class="row">

                                <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 header-phone">
                                    <ul>
                                        <li><span class="fa fa-phone"></span> <a href="tel:<?= get_theme_mod('information_first_tel', '(011)-111-11-11'); ?>" title="Позвонить"><?= get_theme_mod('information_first_tel', '(011)-111-11-11'); ?></a>, </li>
                                        <li><a href="tel:<?= get_theme_mod('information_second_tel', '(011)-111-11-11'); ?>" title="Позвонить"><?= get_theme_mod('information_second_tel', '(011)-111-11-11'); ?></a></li>
                                        <li class="location"><span class="fa fa-map-marker"></span> <?= get_theme_mod('information_country', 'Украина'); ?>, <?= get_theme_mod('information_city', 'г.Харьков'); ?>, <?= get_theme_mod('information_index', '61162'); ?><br><?= get_theme_mod('information_first_address', 'ул.Оддеская 59 (второй этаж)'); ?><br><?= get_theme_mod('information_second_address', 'ст.м.Проспект Гагарина'); ?></li>
                                        <li class="email"><a href="mailto:<?= get_theme_mod('information_email', 'mebel-market@gmail.com'); ?>" title="Написать нам на e-mail"><?= get_theme_mod('information_email', 'mebel-market@gmail.com'); ?></a></li>
                                    </ul>
                                </div>

                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 header-action">
                                    <button data-toggle="modal" data-target="#modal-callback" class="btn btn-info">Обратный звонок</button>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </header>