<?php 
/**
 * @package WordPress
 * @subpackage g-r
 */
get_header(); ?>

<div class="container-fluid">
	<div id="page" class="container">

		<?php if (have_posts()): ?>
        	<div class="row">
            	<?php while (have_posts()): the_post(); ?>
            		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            			<div><h2><?php the_title(); ?></h2></div>
            			<div><?php get_template_part('part/breadcrumbs'); ?></div>
            			<?php if (get_the_content()): ?>
            				<div><?php the_content(); ?></div>
            			<?php endif; ?>
            		</div>
            	<?php endwhile; ?>
            </div>
        <?php endif; ?>

	</div>
</div>

<?php get_footer(); ?>