<?php
/**
 * Author: Vladislav Dneprov
 * Author URI: https://www.linkedin.com/in/vladislav-dneprov
 * Date: 09.12.2016 17:19
 * Version: 1.0.0
 */

function themeslug_customize_register( $wp_customize ) {

    /** section */
    $wp_customize->add_section( 'information', array(
        'title'                 => 'Информация',
        'description'           => 'Информация о сайте',
        'panel'                 => '',
        'priority'              => 160,
        'capability'            => 'edit_theme_options',
        'theme_supports'        => '',
    ) );

    /** tel first */
    $wp_customize->add_setting( 'information_first_tel');
    $wp_customize->add_control( 'information_first_tel', array(
        'type'            => 'tel',
        'priority'        => 10,
        'section'         => 'information',
        'label'           => 'Телефон 1',
        'description'     => 'Введите номер телфона',
        'input_attrs'     => array(
            'placeholder'       => '(099)-999-99-99',
            'id'                => 'input_tel',
        ),
        'active_callback' => 'is_front_page',
    ) );
    /** tel second */
    $wp_customize->add_setting( 'information_second_tel');
    $wp_customize->add_control( 'information_second_tel', array(
        'type'            => 'tel',
        'priority'        => 10,
        'section'         => 'information',
        'label'           => 'Телефон 2',
        'description'     => 'Введите номер телфона',
        'input_attrs'     => array(
            'placeholder'       => '(099)-999-99-99',
            'id'                => 'input_tel',
        ),
        'active_callback' => 'is_front_page',
    ) );
    /** email */
    $wp_customize->add_setting( 'information_email');
    $wp_customize->add_control( 'information_email', array(
        'type'            => 'tel',
        'priority'        => 10,
        'section'         => 'information',
        'label'           => 'E-mail адрес',
        'description'     => 'Введите E-mail',
        'input_attrs'     => array(
            'placeholder'       => 'example@emaple.com',
            'id'                => 'input_email',
        ),
        'active_callback' => 'is_front_page',
    ) );
    /** country */
    $wp_customize->add_setting( 'information_country');
    $wp_customize->add_control( 'information_country', array(
        'type'            => 'tel',
        'priority'        => 10,
        'section'         => 'information',
        'label'           => 'Страна',
        'description'     => 'Страна в которой расположен магазин',
        'input_attrs'     => array(
            'placeholder'       => 'Введите страну',
            'id'                => 'input_country',
        ),
        'active_callback' => 'is_front_page',
    ) );
    /** city */
    $wp_customize->add_setting( 'information_city');
    $wp_customize->add_control( 'information_city', array(
        'type'            => 'tel',
        'priority'        => 10,
        'section'         => 'information',
        'label'           => 'Город',
        'description'     => 'Город в котором расположен магазин',
        'input_attrs'     => array(
            'placeholder'       => 'Введите город',
            'id'                => 'input_country',
        ),
        'active_callback' => 'is_front_page',
    ) );
    /** index */
    $wp_customize->add_setting( 'information_index');
    $wp_customize->add_control( 'information_index', array(
        'type'            => 'text',
        'priority'        => 10,
        'section'         => 'information',
        'label'           => 'Индекс',
        'description'     => 'Введите индекс города',
        'input_attrs'     => array(
            'placeholder'       => 'Введите идекс',
            'id'                => 'input_country',
        ),
        'active_callback' => 'is_front_page',
    ) );
    /** address 1 */
    $wp_customize->add_setting( 'information_first_address');
    $wp_customize->add_control( 'information_first_address', array(
        'type'            => 'tel',
        'priority'        => 10,
        'section'         => 'information',
        'label'           => 'Введите адрес вашего магазина ( первая строчка )',
        'description'     => 'Введите адрес',
        'input_attrs'     => array(
            'placeholder'       => 'Введите адрес',
            'id'                => 'input_country',
        ),
        'active_callback' => 'is_front_page',
    ) );
    /** address 2 */
    $wp_customize->add_setting( 'information_second_address');
    $wp_customize->add_control( 'information_second_address', array(
        'type'            => 'tel',
        'priority'        => 10,
        'section'         => 'information',
        'label'           => 'Введите адрес вашего магазина ( вторая строчка )',
        'description'     => 'Введите адрес',
        'input_attrs'     => array(
            'placeholder'       => 'Введите адрес',
            'id'                => 'input_country',
        ),
        'active_callback' => 'is_front_page',
    ) );
    /** social vk */
    $wp_customize->add_setting( 'information_social_vk');
    $wp_customize->add_control( 'information_social_vk', array(
        'type'            => 'text',
        'priority'        => 10,
        'section'         => 'information',
        'label'           => 'Адрес группы ВК',
        'description'     => 'Введите вк группу',
        'input_attrs'     => array(
            'placeholder'       => 'http://vk.com/group',
        ),
        'active_callback' => 'is_front_page',
    ) );
    /** social odnoclass */
    $wp_customize->add_setting( 'information_social_odnoclass');
    $wp_customize->add_control( 'information_social_odnoclass', array(
        'type'            => 'text',
        'priority'        => 10,
        'section'         => 'information',
        'label'           => 'Адрес группы Одноклассники',
        'description'     => 'Введите дноклассники группу',
        'input_attrs'     => array(
            'placeholder'       => 'http://odnoclassniki.com/group',
        ),
        'active_callback' => 'is_front_page',
    ) );
}
add_action( 'customize_register', 'themeslug_customize_register' );