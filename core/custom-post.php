<?php
/**
 * Author: Vladislav Dneprov
 * Author URI: https://www.linkedin.com/in/vladislav-dneprov
 * Date: 07.12.2016 10:36
 * Version: 1.0.0
 */

/*
 * Desc: Register a post type.
 * Name: slider
 */
function slider_init() {
    $labels = array(
        'name'                  => 'Слайдер',
        'singular_name'         => 'Слайдер',
        'menu_name'             => 'Слайдер',
        'name_admin_bar'        => 'Слайдер',
        'add_new'               => 'Добавить слайд',
        'add_new_item'          => 'Добавить новый слайд',
        'new_item'              => 'Новый слайд',
        'edit_item'             => 'Редактировать слайд',
        'all_items'             => 'Все слайды',
        'search_items'          => 'Найти слайд',
        'not_found'             => 'Слайд не найден',
        'not_found_in_trash'    => 'Слайдов нет в корзине',
    );

    $args = array(
        'labels'                => $labels,
        'public' 				=> true,
        'publicly_queryable' 	=> true,
        'show_ui'               => true,
        'has_archive' 		    => false,
        'menu_position'		    => 4,
        'menu_icon'             => 'dashicons-format-gallery',
        'supports'              => array( 'title', 'thumbnail', 'editor' ),
    );

    register_post_type( 'slider' , $args);
}
add_action( 'init', 'slider_init' );

/*
 * Customize default column
 */
function slider_custom_column($defaults) {

    $columns = array(
        'cb'	 		=> '<input type="checkbox" />',
        'img'           => '<span class="dashicons dashicons-format-image"></span>',
        'title'			=> 'Название',
        'desc'			=> 'Текст на слайдере',
        'date'			=> 'Дата создания <span class="dashicons dashicons-calendar"></span>',
    );

    return $columns;
}
add_filter( 'manage_slider_posts_columns', 'slider_custom_column' );

/*
 * Get value to column fields
 */
function slider_get_column_value($column, $post_id) {
    if( $column == 'img' )
        echo get_the_post_thumbnail( $post_id, array( 60, 60 ) );

    if( $column == 'desc' )
        echo get_the_content( $post_id );
}
add_action( 'manage_slider_posts_custom_column', 'slider_get_column_value', 10, 2 );


/*
 * Desc: Register a post type.
 * Name: slider
 */
function product_init() {
    $labels = array(
        'name'                  => 'Товары',
        'singular_name'         => 'Товары',
        'menu_name'             => 'Товары',
        'name_admin_bar'        => 'Товары',
        'add_new'               => 'Добавить товар',
        'add_new_item'          => 'Добавить новый товар',
        'new_item'              => 'Новый товар',
        'edit_item'             => 'Редактировать товар',
        'all_items'             => 'Все товары',
        'search_items'          => 'Найти товар',
        'not_found'             => 'Товар не найден',
        'not_found_in_trash'    => 'Товаров нет в корзине',
    );

    $args = array(
        'labels'                => $labels,
        'public' 				=> true,
        'publicly_queryable' 	=> true,
        'show_ui'               => true,
        'has_archive' 		    => false,
        'menu_position'		    => 4,
        'menu_icon'             => 'dashicons-products',
        'supports'              => array( 'title', 'thumbnail' ),
    );

    register_post_type( 'product' , $args);
}
add_action( 'init', 'product_init' );

/*
 * Add extra fields
 */
function product_extra_fields() {
    add_meta_box(
        'general_settings_product',
        'Параметры товара',
        'product_fields',
        'product',
        'normal',
        'high'
    );
}
add_action( 'add_meta_boxes', 'product_extra_fields', 1 );

function product_images_extra_fields() {
    add_meta_box(
        'side_images_product',
        'Изображения товара',
        'product_side_images_fields',
        'product',
        'side',
        'low'
    );
}
add_action( 'add_meta_boxes', 'product_images_extra_fields', 1 );

/*
 * Customize default column
 */
function product_custom_column($defaults) {

    $columns = array(
        'cb'	 		=> '<input type="checkbox" />',
        'img'           => '<span class="dashicons dashicons-format-image"></span>',
        'title'			=> 'Название',
        'price'			=> 'Цена',
        'short_desc'	=> 'Краткое описание',
        'category'      => 'Категория',
        'views'         => '<span class="dashicons dashicons-visibility"></span> Просмотры',
        'date'			=> 'Дата создания <span class="dashicons dashicons-calendar"></span>',
    );

    return $columns;
}
add_filter( 'manage_product_posts_columns', 'product_custom_column' );

/*
 * Get value to column fields
 */
function product_get_column_value($column, $post_id) {
    if ( $column == 'img' )
        if (get_the_post_thumbnail( $post_id, array( 60, 60 ) ))
            echo get_the_post_thumbnail( $post_id, array( 60, 60 ) );
        else
            echo 'Изображение не задано.';

    if ( $column == 'price' )
        if (get_post_meta( $post_id, 'price', 1 ))
            echo number_format(get_post_meta( $post_id, 'price', 1 ), 2, '.', ' ').' грн.';
        else
            echo 'Цена не задана.';

    if ( $column == 'views' ) {
        echo getPostViews( $post_id );
    }

    if ($column == 'category') {
        $taxonomy = 'product-category';

        $businesses = get_the_terms($post_id,$taxonomy);
        if (is_array($businesses)) {
            foreach($businesses as $key => $business) {
                $edit_link = get_term_link($business,$taxonomy);
                $businesses[$key] = '<a href="'.$edit_link.'">' . $business->name . '</a>';
            }
            echo implode(' | ',$businesses);
        }
    }

    if ( $column == 'short_desc' )
        if (get_post_meta( $post_id, 'short_desc', 1 ))
            echo wp_trim_words(get_post_meta( $post_id, 'short_desc', 1 ), 20, '...');
        else
            echo 'Краткое описание не задано.';
}
add_action( 'manage_product_posts_custom_column', 'product_get_column_value', 10, 2 );

/*
 * Add taxonomy's filter
 */
function taxonomy_filter_product() {
    global $typenow;
    global $wp_query;
    if ( $typenow == 'product' ) {
        $taxonomy = 'product-category';
        $business_taxonomy = get_taxonomy( $taxonomy );
        wp_dropdown_categories(array(
            'show_option_all' =>  'Показать все '.$business_taxonomy->label,
            'taxonomy'        =>  $taxonomy,
            'name'            =>  $taxonomy,
            'orderby'         =>  'name',
            'selected'        =>  $wp_query->query['product-category'],
            'hierarchical'    =>  true,
            'depth'           =>  3,
            'show_count'      =>  true,
            'hide_empty'      =>  false,
        ));
    }
}
add_action( 'restrict_manage_posts', 'taxonomy_filter_product' );

/*
 * Add query
 */
function convert_product_category_id_to_taxonomy_term_in_query($query) {
    global $pagenow;
    $post_type = 'product';
    $taxonomy  = 'product-category';
    $q_vars    = &$query->query_vars;
    if ( $pagenow == 'edit.php' && isset($q_vars['post_type']) && $q_vars['post_type'] == $post_type && isset($q_vars[$taxonomy]) && is_numeric($q_vars[$taxonomy]) && $q_vars[$taxonomy] != 0 ) {
        $term = get_term_by('id', $q_vars[$taxonomy], $taxonomy);
        $q_vars[$taxonomy] = $term->slug;
    }
}
add_filter('parse_query','convert_product_category_id_to_taxonomy_term_in_query');

function product_side_images_fields($post) {
    wp_enqueue_media();
    $images = json_decode(get_post_meta( $post->ID, 'images', 1 ));
    ?>

    <?php if (isset($images)): ?>
        <?php foreach ($images as $key => $images): ?>
            <img style="width:100%;" src="<?= $images; ?>" alt="" />
        <?php endforeach; ?>
    <?php endif; ?>

    <p>
        <button class="button button-primary add_images">Добавить изображения</button>
        <input class="input_images" type="hidden" name="product[images]" value='<?= get_post_meta( $post->ID, 'images', 1 ); ?>'>
    </p>
    <script>
        jQuery(document).ready(function(){

            jQuery('.add_images').click(function(e) {
                e.preventDefault();
                var image = wp.media({
                    title: 'Загрузить изображения',
                    multiple: true
                }).open()
                    .on('select', function(e){
                        // This will return the selected image from the Media Uploader, the result is an object
                        var uploaded_image = image.state().get('selection').first();
                        // We convert uploaded_image to a JSON object to make accessing it easier
                        // Output to the console uploaded_image
                        var array_object = image.state().get('selection').toJSON();

                        var image_url_array = [];

                        for(var i = 0; i < array_object.length; i++) {
                            image_url_array.push(array_object[i].url);
                        }

                        var image_urls = JSON.stringify(image_url_array);
                        // Let's assign the url value to the input field
                        jQuery('.input_images').val(image_urls);
                    });
            });
        });
    </script>

    <?php
}

/*
 * Extra fields
 */
function product_fields( $post ){
    ?>

    <p>
        <label>Цена: </label>
        <input type="text" placeholder="Цена" name="product[price]" value="<?= get_post_meta( $post->ID, 'price', 1 ) ?>"> грн.
    </p>

    <p>
        <label>Цена со скидкой: </label>
        <input type="text" placeholder="Скидочная цена" name="product[price_sale]" value="<?= get_post_meta( $post->ID, 'price_sale', 1 ) ?>"> грн.
    </p>

    <label>Краткое описание: </label>
    <p>
        <textarea name="product[short_desc]" style="width:100%; height: 100px;" placeholder="Краткое описание товара"><?= get_post_meta( $post->ID, 'short_desc', 1 ) ?></textarea>
    </p>

    <label>Подробное описание: </label>
    <?php
    wp_editor( get_post_meta( $post->ID, 'full_desc', 1 ), 'wpeditor', array(
        'media_buttons'     => false,
        'textarea_name'     => 'product[full_desc]',
        'textarea_rows'     => '10',
    ) );
    ?>

    <?php
    $harak = json_decode(get_post_meta( $post->ID, 'product_harak', 1 ));
    ?>
    <hr>
    <h2>Характеристики <button id="add_harak" class="button button-primary">Добавить новую</button></h2>
    <p>
    <table id="table_harak">
        <tr>
            <th>Название</th>
            <th>Значение</th>
            <th>Действия</th>
        </tr>
        <?php if ($harak): ?>
            <?php foreach ($harak as $key => $hara): ?>
                <tr>
                    <td><input type="text" value="<?= $hara->name; ?>" name="product_harak_name[]" /></td>
                    <td><input type="text" value="<?= $hara->value; ?>" name="product_harak_value[]" /></td>
                    <td><button class="button button-primary delete_harak">Удалить</button></td>
                </tr>
            <?php endforeach; ?>
        <?php endif; ?>
    </table>
    </p>
    <script>
        jQuery(document).ready(function($){

            $('.delete_harak').click(function (e_delete) {
                e_delete.preventDefault();
                $(this).parent().parent().remove();
            });

            $('#add_harak').click(function(e){
                var count = $('#table_harak tr').length;
                e.preventDefault();
                $('#table_harak').append(`
                    <tr>
                        <td><input type="text" name="product_harak_name[]" /></td>
                        <td><input type="text" name="product_harak_value[]" /</td>
                        <td><button class="button button-primary delete_harak">Удалить</button></td>
                    </tr>
                `);
            });
        });
    </script>

    <input type="hidden" name="extra_fields_nonce" value="<?php echo wp_create_nonce(__FILE__); ?>" />

    <input name="save" type="submit" class="button button-primary button-large" value="Сохранить">

    <?php
}

/*
 * Save product extra fields
 */
function product_update( $post_id ) {
    if ( !isset($_POST['extra_fields_nonce']) || !wp_verify_nonce($_POST['extra_fields_nonce'], __FILE__) ) return false;
    if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE  ) return false;
    if ( !current_user_can('edit_post', $post_id) ) return false;

    if( !isset($_POST['product']) ) return false;

    if ( isset($_POST['product_harak_name']) && isset($_POST['product_harak_value']) ) {

        $data = array();

        foreach ($_POST['product_harak_name'] as $key => $value) {
            $data[$key + 1] = array('name' => $_POST['product_harak_name'][$key], 'value' => $_POST['product_harak_value'][$key]);
        }

        $value = json_encode($data, JSON_UNESCAPED_UNICODE);
        update_post_meta($post_id, 'product_harak', $value);
    }

    foreach( $_POST['product'] as $key=>$value ){
        if( empty($value) ){
            delete_post_meta( $post_id, $key );
            continue;
        }

        update_post_meta($post_id, $key, $value);
    }
    return $post_id;
}
add_action( 'save_post', 'product_update', 0 );


function create_taxonomy_product(){

    $labels = array(
        'name'              => 'Категории товара',
        'singular_name'     => 'Категория товара',
        'search_items'      => 'Найти категории',
        'all_items'         => 'Все категории',
        'parent_item'       => 'Категория',
        'parent_item_colon' => 'Категория:',
        'edit_item'         => 'Редактировать данные категории товара',
        'update_item'       => 'Обновить данные категории товара',
        'add_new_item'      => 'Добавить новую категорию товара',
        'new_item_name'     => 'Новая категория товара',
        'menu_name'         => 'Категория',
    );

    $args = array(
        'label'                 => '',
        'labels'                => $labels,
        'description'           => '',
        'public'                => true,
        'publicly_queryable'    => null,
        'show_in_nav_menus'     => true,
        'show_ui'               => true,
        'show_tagcloud'         => true,
        'hierarchical'          => true,
        'update_count_callback' => '',
        'rewrite'               => true,
        'capabilities'          => array(),
        'meta_box_cb'           => null,
        'show_admin_column'     => false,
        '_builtin'              => false,
        'show_in_quick_edit'    => null,
    );
    register_taxonomy('product-category', array('product'), $args );
}
add_action('init', 'create_taxonomy_product');


/*
 * Desc: Register a post type.
 * Name: partner
 */
function partner_init() {
    $labels = array(
        'name'                  => 'Партнёры',
        'singular_name'         => 'Партнёры',
        'menu_name'             => 'Партнёр',
        'name_admin_bar'        => 'Партнёр',
        'add_new'               => 'Добавить партнёра',
        'add_new_item'          => 'Добавить нового партнёра',
        'new_item'              => 'Новый партнёр',
        'edit_item'             => 'Редактировать партнёра',
        'all_items'             => 'Список партнёров',
        'search_items'          => 'Найти партнёра',
        'not_found'             => 'Партнёр не найден',
        'not_found_in_trash'    => 'Партнёров нет в корзине',
    );

    $args = array(
        'labels'                => $labels,
        'public' 				=> true,
        'publicly_queryable' 	=> true,
        'show_ui'               => true,
        'has_archive' 		    => false,
        'menu_position'		    => 4,
        'menu_icon'             => 'dashicons-groups',
        'supports'              => array( 'title', 'thumbnail' ),
    );

    register_post_type( 'partner' , $args);
}
add_action( 'init', 'partner_init' );

/*
 * Add extra fields
 */
function partner_extra_fields() {
    add_meta_box(
        'general_settings_product',
        'Параметры',
        'partner_fields',
        'partner',
        'normal',
        'high'
    );
}
add_action( 'add_meta_boxes', 'partner_extra_fields', 1 );

/*
 * Extra fields
 */
function partner_fields( $post ){
    ?>

    <p>
        <label>Ссылка на сайт партнёра: </label>
        <input style="width:50%;" type="url" placeholder="Ссылка" name="partner[url]" value="<?= get_post_meta( $post->ID, 'url', 1 ) ?>">
    </p>

    <input type="hidden" name="extra_fields_nonce" value="<?php echo wp_create_nonce(__FILE__); ?>" />

    <input name="save" type="submit" class="button button-primary button-large" value="Сохранить">

    <?php
}

/*
 * Save product extra fields
 */
function partner_update( $post_id ) {
    if ( !isset($_POST['extra_fields_nonce']) || !wp_verify_nonce($_POST['extra_fields_nonce'], __FILE__) ) return false;
    if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE  ) return false;
    if ( !current_user_can('edit_post', $post_id) ) return false;

    if( !isset($_POST['partner']) ) return false;

    foreach( $_POST['partner'] as $key=>$value ){
        if( empty($value) ){
            delete_post_meta( $post_id, $key );
            continue;
        }

        update_post_meta($post_id, $key, $value);
    }
    return $post_id;
}
add_action( 'save_post', 'partner_update', 0 );

/*
 * Customize default column
 */
function partner_custom_column($defaults) {

    $columns = array(
        'cb'	 		=> '<input type="checkbox" />',
        'img'           => '<span class="dashicons dashicons-format-image"></span>',
        'title'			=> 'Название',
        'date'			=> 'Дата создания <span class="dashicons dashicons-calendar"></span>',
    );

    return $columns;
}
add_filter( 'manage_partner_posts_columns', 'partner_custom_column' );

/*
 * Get value to column fields
 */
function partner_get_column_value($column, $post_id) {
    if( $column == 'img' )
        echo get_the_post_thumbnail( $post_id, 'full', array('style' => 'width:220px; height: auto;') );
}
add_action( 'manage_partner_posts_custom_column', 'partner_get_column_value', 10, 2 );