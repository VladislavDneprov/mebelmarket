<?php
/**
 * Author: Vlad Dneprov, https://www.linkedin.com/in/vladislav-dneprov
 * Author URI: http://jaguar-team.com
 * Date: 11.11.2016
 * Version: 1.0.0
 */

// load_theme_textdomain( 'gr', get_template_directory() . '/languages' );

require_once ( __DIR__.'/core/custom-post.php' );
require_once ( __DIR__.'/core/breadcrumbs.php' );
require_once ( __DIR__.'/core/settings.php' );
// require_once ( __DIR__.'/core/user-functions.php' );

/** add javascript **/
add_action( 'wp_enqueue_scripts', 'add_scripts' );
function add_scripts() {
	
	/** lib **/
	wp_enqueue_script( 'jquery' );
	wp_enqueue_script( 'bootstrap', 		get_template_directory_uri().'/js/lib/bootstrap.min.js', array( 'jquery' ), '', true );
	wp_enqueue_script( 'slick', 			get_template_directory_uri().'/js/lib/slick/slick.min.js', array( 'jquery' ), '', true );
	wp_enqueue_script( 'masked', 			get_template_directory_uri().'/js/lib/jquery.inputmask.bundle.min.js', array( 'jquery' ), '', true );
    wp_enqueue_media();
	/** custom **/
	wp_enqueue_script( 'main', 				get_template_directory_uri().'/js/main.js', array( 'jquery' ), '', true );

}

/** add css **/
add_action( 'wp_print_styles', 'add_styles' );
function add_styles() {

	wp_localize_script('main', 'myajax',
		array(
			'url' => admin_url('admin-ajax.php')
		)
	);

	/** lib **/
	wp_enqueue_style( 'bootstrap', 			get_template_directory_uri().'/css/lib/bootstrap.min.css' );
	wp_enqueue_style( 'slick', 				get_template_directory_uri().'/js/lib/slick/slick.css' );
	wp_enqueue_style( 'slick-theme', 		get_template_directory_uri().'/js/lib/slick/slick-theme.css' );

	/** fonts **/
	wp_enqueue_style( 'font-awesome', 		get_template_directory_uri().'/css/lib/font-awesome.min.css' );

	/** custom **/
	wp_enqueue_style( 'main', 				get_template_directory_uri().'/style.css' );
	wp_enqueue_style( 'global', 			get_template_directory_uri().'/css/global.css' );
    wp_enqueue_style( 'global-tablet', 		get_template_directory_uri().'/css/global-tablet.css' );
    wp_enqueue_style( 'global-mobile', 		get_template_directory_uri().'/css/global-mobile.css' );

	if ( is_home() ) {
        wp_enqueue_style( 'index', 			get_template_directory_uri().'/css/index.css' );
        wp_enqueue_style( 'index-tablet', 	get_template_directory_uri().'/css/index-tablet.css' );
        wp_enqueue_style( 'index-mobile', 	get_template_directory_uri().'/css/index-mobile.css' );
    }

	if ( is_single() ) {
		wp_enqueue_style( 'single', 		get_template_directory_uri().'/css/single.css' );
        wp_enqueue_style( 'single-tablet', 	get_template_directory_uri().'/css/single-tablet.css' );
        wp_enqueue_style( 'single-mobile', 	get_template_directory_uri().'/css/single-mobile.css' );
	}

	if ( is_404() ) {
	    wp_enqueue_style( 'not-found', 		get_template_directory_uri().'/css/not-found.css' );
	}

	if ( is_singular( 'product' ) ) {
		wp_enqueue_style( 'single-product', 			get_template_directory_uri().'/css/single-product.css' );
		wp_enqueue_style( 'single-product-tablet', 		get_template_directory_uri().'/css/single-product-tablet.css' );
		wp_enqueue_style( 'single-product-mobile', 		get_template_directory_uri().'/css/single-product-mobile.css' );
	}

	if ( is_tax( 'product-category' ) ) {
		wp_enqueue_style( 'product-category', 			get_template_directory_uri().'/css/product-category.css' );
		wp_enqueue_style( 'product-category-table', 	get_template_directory_uri().'/css/product-category-tablet.css' );
		wp_enqueue_style( 'product-category-mobile', 	get_template_directory_uri().'/css/product-category-mobile.css' );
	}
}

add_theme_support( 'post-thumbnails' ); # add thubnails

remove_filter( 'the_content', 'wpautop' );

/** registration nav-menu **/
register_nav_menus(array(
	'sidebar-info' 		=> 'Блок информации',
));

function getPostViews($postID){
	$count_key = 'views';
	$count = get_post_meta($postID, $count_key, true);
	if($count==''){
		delete_post_meta($postID, $count_key);
		add_post_meta($postID, $count_key, '0');
		return "0";
	}
	return $count;
}
function setPostViews($postID) {
	$count_key = 'views';
	$count = get_post_meta($postID, $count_key, true);
	if($count==''){
		$count = 0;
		delete_post_meta($postID, $count_key);
		add_post_meta($postID, $count_key, '0');
	}else{
		$count++;
		update_post_meta($postID, $count_key, $count);
	}
}