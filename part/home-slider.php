<?php
/**
 * Author: Vladislav Dneprov
 * Author URI: https://www.linkedin.com/in/vladislav-dneprov
 * Date: 07.12.2016 10:58
 * Version: 1.0.0
 */

$args = array(
    'numberposts'     => -1,
    'offset'          => 0,
    'category'        => '',
    'orderby'         => 'post_date',
    'order'           => 'ASC',
    'include'         => '',
    'exclude'         => '',
    'meta_key'        => '',
    'meta_value'      => '',
    'post_type'       => 'slider',
    'post_status'     => 'publish'
);

$slides = get_posts($args);

?>

<?php if ($slides): ?>
    <div class="slider-home">
    <?php foreach ($slides as $number => $slide): ?>
        <div class="slider-home-container">
            <?= get_the_post_thumbnail($slide->ID, 'full', array('alt' => $slide->post_title));  ?>
            <div class="desc"><?= $slide->post_content; ?></div>
        </div>
    <?php endforeach; ?>
    </div>
<?php endif; ?>
