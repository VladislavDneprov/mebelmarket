<?php
/**
 * Author: Vladislav Dneprov
 * Author URI: https://www.linkedin.com/in/vladislav-dneprov
 * Date: 07.12.2016 11:10
 * Version: 1.0.0
 */

$args_features = array(
    'numberposts'     => 6,
    'orderby'         => 'meta_value_num',
    'order'           => 'DESC',
    'meta_key'        => 'views',
    'post_type'       => 'product',
    'post_status'     => 'publish'
);

$args_new = array(
    'numberposts'     => 6,
    'orderby'         => 'date',
    'order'           => 'DESC',
    'post_type'       => 'product',
    'post_status'     => 'publish'
);

$products_features = get_posts($args_features);
$products_news = get_posts($args_new);

?>
    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#popular" aria-controls="popular" role="tab" data-toggle="tab">Популярные</a></li>
        <li role="presentation"><a href="#new" aria-controls="new" role="tab" data-toggle="tab">Новые поступления</a></li>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="popular">
            <div class="row">
                <?php $current_product_number = 1; ?>
                <?php foreach ($products_features as $key => $product): ?>
                    <?php
                    $data_end = strtotime($product->post_date.' +90 day');
                    $current_end = strtotime(date('Y-m-d H:m:s'));
                    if ($current_end < $data_end)
                        $new = true;
                    else
                        $new = false;
                    ?>
                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                        <div class="container-product <?= ($new ? 'new' : ''); ?> <?= (get_post_meta( $product->ID, 'price_sale', 1 ) ? 'discount' : ''); ?>">
                            <div class="img">
                                <a href="<?= get_permalink($product->ID); ?>" title="<?= $product->post_title; ?>">
                                    <div class="div-img"><?= get_the_post_thumbnail( $product->ID ); ?></div>
                                </a>
                            </div>
                            <div class="content">
                                <div class="title"><a href="#"><?= $product->post_title; ?></a></div>
                                <div class="desc"><?= wp_trim_words(get_post_meta( $product->ID, 'short_desc', 1 ), 20, '...'); ?></div>
                                <div class="price">
                                    <?php if (get_post_meta( $product->ID, 'price_sale', 1 ) && get_post_meta( $product->ID, 'price', 1 )): ?>
                                        <?php
                                        $sale = (get_post_meta( $product->ID, 'price', 1 ) - get_post_meta( $product->ID, 'price_sale', 1 )) / (get_post_meta( $product->ID, 'price', 1 ) / 100);
                                        $sale = round($sale, 0, PHP_ROUND_HALF_UP);
                                        ?>
                                        <?= number_format(get_post_meta( $product->ID, 'price_sale', 1 ), 2, '.', ' '); ?> грн. <span class="price-percent-reduction">-<?= $sale ?>%</span>
                                        <br><del class="old-price"><?= number_format(get_post_meta( $product->ID, 'price', 1 ), 2, '.', ' '); ?> грн.</del>
                                    <?php else: ?>
                                        <?php if (get_post_meta( $product->ID, 'price', 1 )): ?>
                                            <?= number_format(get_post_meta( $product->ID, 'price', 1 ), 2, '.', ' '); ?> грн.
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </div>
                                <div class="action">
                                    <button data-toggle="modal" data-target="#modal-order" class="btn btn-default buy">Купить</button>
                                    <a href="<?= get_permalink($product->ID); ?>" title="Подробнее" class="btn btn-default">Подробнее</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                        if ($current_product_number == 3) {
                            $current_product_number = 1;
                            echo '<div class="clearfix visible-md-block visible-lg-block"></div>';
                        }
                        else {
                            $current_product_number++;
                        }
                    ?>
                <?php endforeach; ?>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="new">
            <div class="row">
                <?php $current_product_number = 1; ?>
                <?php foreach ($products_news as $key => $product): ?>
                    <?php
                    $data_end = strtotime($product->post_date.' +90 day');
                    $current_end = strtotime(date('Y-m-d H:m:s'));
                    if ($current_end < $data_end)
                        $new = true;
                    else
                        $new = false;
                    ?>
                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                        <div class="container-product <?= ($new ? 'new' : ''); ?> <?= (get_post_meta( $product->ID, 'price_sale', 1 ) ? 'discount' : ''); ?>">
                            <div class="img">
                                <a href="<?= get_permalink($product->ID); ?>" title="<?= $product->post_title; ?>">
                                    <div class="div-img"><?= get_the_post_thumbnail( $product->ID ); ?></div>
                                </a>
                            </div>
                            <div class="content">
                                <div class="title"><a href="#"><?= $product->post_title; ?></a></div>
                                <div class="desc"><?= wp_trim_words(get_post_meta( $product->ID, 'short_desc', 1 ), 20, '...'); ?></div>
                                <div class="price">
                                    <?php if (get_post_meta( $product->ID, 'price_sale', 1 ) && get_post_meta( $product->ID, 'price', 1 )): ?>
                                        <?php
                                        $sale = (get_post_meta( $product->ID, 'price', 1 ) - get_post_meta( $product->ID, 'price_sale', 1 )) / (get_post_meta( $product->ID, 'price', 1 ) / 100);
                                        ?>
                                        <?= number_format(get_post_meta( $product->ID, 'price_sale', 1 ), 2, '.', ' '); ?> грн. <span class="price-percent-reduction">-<?= $sale ?>%</span>
                                        <br><del class="old-price"><?= number_format(get_post_meta( $product->ID, 'price', 1 ), 2, '.', ' '); ?> грн.</del>
                                    <?php else: ?>
                                        <?php if (get_post_meta( $product->ID, 'price', 1 )): ?>
                                            <?= number_format(get_post_meta( $product->ID, 'price', 1 ), 2, '.', ' '); ?> грн.
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </div>
                                <div class="action">
                                    <button class="btn btn-default buy">Купить</button>
                                    <a href="<?= get_permalink($product->ID); ?>" title="Подробнее" class="btn btn-default">Подробнее</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                        if ($current_product_number == 3) {
                            $current_product_number = 1;
                            echo '<div class="clearfix visible-md-block visible-lg-block"></div>';
                        }
                        else {
                            $current_product_number++;
                        }
                    ?>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
