<?php
/**
 * Author: Vladislav Dneprov
 * Author URI: https://www.linkedin.com/in/vladislav-dneprov
 * Date: 08.12.2016 10:46
 * Version: 1.0.0
 */
global $wp_query;

/** pagination */
$current_term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );

$current = ( get_query_var('paged') ? get_query_var('paged') : 1 );
$total = $wp_query->max_num_pages;

$args_pag = array(
    //'base'         => '%_%',
    //'format'       => '?paged=%#%',
    'total'        => $total,
    'current'      => $current,
    //'show_all'     => false,
    'end_size'     => 3,
    'mid_size'     => 1,
    'prev_next'    => True,
    'prev_text'    => '<span class="fa fa-arrow-left"><span>',
    'next_text'    => '<span class="fa fa-arrow-right"><span>',
    //'type'         => 'plain',
    //'add_args'     => False,
    //'add_fragment' => '',
    //'before_page_number' => '',
    //'after_page_number'  => ''
);

$pag_links = paginate_links( $args_pag );

get_header(); ?>

<div id="category" class="container-fluid">
    <div class="container">

        <!-- category info -->
        <div class="row category-header">
            <div class="category-header-content">
                <div class="title col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <?= single_cat_title( '', false ); ?>
                </div>
                <div class="breadcrumb col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <?php breadcrumbs('/ ', array('home' => '<span class="fa fa-home"></span> ')); ?>
                </div>
                <div class="desc col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <?= term_description(); ?>
                </div>
            </div>
        </div>
        <!-- /category info -->

        <?php if ( have_posts() ) : ?>
        <!-- list product -->
        <div class="row list-products">
                    <?php $current_product_number = 1; ?>
                    <?php while ( have_posts() ) : the_post(); ?>
                        <?php
                            $data_end = strtotime(get_the_date().' +90 day');
                            $current_end = strtotime(date('Y-m-d H:m:s'));
                            if ($current_end < $data_end)
                                $new = true;
                            else
                                $new = false;
                        ?>
                        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                            <div class="container-product <?= ($new ? 'new' : ''); ?> <?= (get_post_meta( get_the_id(), 'price_sale', 1 ) ? 'discount' : ''); ?>">
                                <div class="img">
                                    <a href="<?= get_permalink($product->ID); ?>" title="<?= get_the_title(); ?>">
                                        <div class="div-img"><?= get_the_post_thumbnail( get_the_id() ); ?></div>
                                    </a>
                                </div>
                                <div class="content">
                                    <div class="title"><a href="#"><?= get_the_title(); ?></a></div>
                                    <div class="desc"><?= wp_trim_words(get_post_meta( get_the_id(), 'short_desc', 1 ), 20, '...'); ?></div>
                                    <div class="price">
                                        <?php if (get_post_meta( get_the_id(), 'price_sale', 1 ) && get_post_meta( get_the_id(), 'price', 1 )): ?>
                                            <?php
                                            $sale = (get_post_meta( get_the_id(), 'price', 1 ) - get_post_meta( get_the_id(), 'price_sale', 1 )) / (get_post_meta( get_the_id(), 'price', 1 ) / 100);
                                            ?>
                                            <?= number_format(get_post_meta( get_the_id(), 'price_sale', 1 ), 2, '.', ' '); ?> грн. <span class="price-percent-reduction">-<?= $sale ?>%</span>
                                            <br><del class="old-price"><?= number_format(get_post_meta( get_the_id(), 'price', 1 ), 2, '.', ' '); ?> грн.</del>
                                        <?php else: ?>
                                            <?php if (get_post_meta( get_the_id(), 'price', 1 )): ?>
                                                <?= number_format(get_post_meta( get_the_id(), 'price', 1 ), 2, '.', ' '); ?> грн.
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    </div>
                                    <div class="action">
                                        <button class="btn btn-default buy">Купить</button>
                                        <a href="<?= get_permalink(get_the_id()); ?>" title="Подробнее" class="btn btn-default">Подробнее</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                            if ($current_product_number == 4) {
                                $current_product_number = 1;
                                echo '<div class="clearfix visible-xs-block visible-sm-block visible-md-block visible-lg-block"></div>';
                            }
                            else {
                                $current_product_number++;
                            }
                        ?>
                    <?php endwhile; ?>
                </div>
            <?php else : ?>
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                    <p>Товары не найдены.</p>
                </div>
            </div>
            <?php endif; ?>
            <!-- /list product -->

            <!-- pagination -->
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pagination">
                    <?= $pag_links; ?>
                </div>
            </div>
            <!-- /pagination -->
        </div>
</div>
<?php get_footer(); ?>
